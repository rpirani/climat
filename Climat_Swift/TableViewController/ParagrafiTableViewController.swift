//
//  ParagrafiTableViewController.swift
//  Climat_Swift
//
//  Created by Rudi on 02/02/2020.
//  Copyright © 2020 Rudi. All rights reserved.

import UIKit

class ParagrafiTableViewController: UITableViewController { //UISearchResultsUpdating
    
    var resultSearchController: UISearchController?
    var listaFiltrata = [Paragrafo]()
    var paragrafi = [Paragrafo]()
    var indSezione = 0;
    
    @IBOutlet weak var menuButton: UIButton!
    
    var documento = Documento()
    var fileMgr = FileManager()
    
    @IBAction func svuotaDocumento(_ sender: UIButton) {
//        let alert = UIAlertController(title: "Vuoi creare un nuovo documento?", message: "", preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "Si", style: .destructive, handler: {
//            action in self.documento = documento.creaNuovoDocumento(enmTipoDocumento: enmTipoDocumento)
//            self.paragrafi = self.documento.sezioni.paragrafi
//            self.tableView.reloadData()
//
//        }))
//        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
//        self.present(alert, animated: true, completion: nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if !documento.salva(nomeFile: documento.nomeFile) {
            let alert = UIAlertController(title: "Attenzione", message: "Salvataggio dati non riuscito", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        paragrafi = documento.sezioni[indSezione].paragrafi
        self.tableView.reloadData()
        
//        self.title = getDataString(of: documento.dataRevisione, with: "dd/MM/yyyy")
        self.title = documento.sezioni[indSezione].titolo
                
        if self.documento.enmTipoDoc != .rapportoControlloTipo1 {
            menuButton.isHidden = true
        }
        
//        aggiungiSearchBar()
        

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
    }
    
    // MARK: - Action Sheet
    
    @IBAction func showActionSheet(_ sender: Any) {
        let optionMenu = UIAlertController(title: nil, message: "", preferredStyle: .actionSheet)
                
            let deleteAction = UIAlertAction(title: "Nuovo documento", style: .default, handler: chiediNuovoDocumento(alert:))
            let sendAction = UIAlertAction(title: "Invia documento", style: .default, handler: inviaDocumento(alert:))
            let saveAction = UIAlertAction(title: "Sposta in Storico", style: .default, handler: salvaDocumento(alert:))
            
            let cancelAction = UIAlertAction(title: "Annulla", style: .cancel)
                
            optionMenu.addAction(sendAction)
            if self.documento.nomeFile == "" {
                optionMenu.addAction(deleteAction)
                optionMenu.addAction(saveAction)
            }
            optionMenu.addAction(cancelAction)
                
            self.present(optionMenu, animated: true, completion: nil)
        }
    
    func creaNuovoDocumento() {
        self.documento = self.documento.creaNuovoDocumento(enmTipoDocumento: self.documento.enmTipoDoc)
        self.paragrafi = self.documento.sezioni[self.indSezione].paragrafi
        self.tableView.reloadData()
    }
    
    func chiediNuovoDocumento(alert: UIAlertAction!) {
        let alert = UIAlertController(title: "Vuoi creare un nuovo documento?", message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Si", style: .destructive, handler: {
            action in self.creaNuovoDocumento()
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func inviaDocumento(alert: UIAlertAction!) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "PDFViewController") as! PDFViewController
        vc.documento = self.documento
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func salvaDocumento(alert: UIAlertAction!) {
        // Se il documento ha gia un nome assegnato, lo sovrascrivo con quel nome
        if self.documento.nomeFile != "" {
            if !self.documento.salva(nomeFile: documento.nomeFile) {
                // Alert salvataggio non riuscito
                self.mostraAlert(titolo: "Attenzione", messaggio: "Salvataggio dati non riuscito")
            } else {
                // Se il salvataggio avviene correttamente, creo un nuovo documento, in modo da spostare il doc appena creato in Storico
                self.creaNuovoDocumento()
            }
            return
        }
        // Se il documento NON ha un nome assegnato, chiedo il nome e lo salvo con il nome immesso dall'utente
        let alert = UIAlertController(title: "Salvataggio documento", message: "Assegna un nome al documento", preferredStyle: .alert)

        alert.addTextField { (textField) in
            //textField.text = "Some default text"
        }

        alert.addAction(UIAlertAction(title: "Annulla", style: .cancel))
        alert.addAction(UIAlertAction(title: "Sposta", style: .default, handler: { [weak alert] (_) in
            let text = alert?.textFields![0].text
            // Se il testo è vuoto
            if text == "" {
                self.mostraAlert(titolo: "Attenzione", messaggio: "Assegnare un nome al documento")
            } else {
                // Altrimenti salvo
                if !self.documento.salva(nomeFile: text!) {
                    // Alert salvataggio non riuscito
                    self.mostraAlert(titolo: "Attenzione", messaggio: "Salvataggio dati non riuscito")
                } else {
                    // Se il salvataggio avviene correttamente, creo un nuovo documento, in modo da spostare il doc appena creato in Storico
                    self.creaNuovoDocumento()
                }
            }
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func mostraAlert(titolo: String, messaggio: String){
        let alert = UIAlertController(title: titolo, message: messaggio, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func getDataString(of myDate: Date, with format: String) -> String {
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = format//"yyyy-MM-dd HH:mm:ss"

        return formatter.string(from: myDate) // string purpose I add here
        // convert your string to date
    }
    
//
    //MARK: - SearchBar
    
//    func aggiungiSearchBar(){
//        self.resultSearchController = ({
//            // creo un oggetto di tipo UISearchController
//            let controller = UISearchController(searchResultsController: nil)
//            // rimuove la tableView di sottofondo in modo da poter successivamente visualizzare gli elementi cercati
//            controller.dimsBackgroundDuringPresentation = false
//
//            // il searchResultsUpdater, ovvero colui che gestirà gli eventi di ricerca, sarà la ListaTableViewController (o self)
//            controller.searchResultsUpdater = self as? UISearchResultsUpdating
//
//            // impongo alla searchBar, contenuta all'interno del controller, di adattarsi alle dimensioni dell'applicazioni
//            controller.searchBar.sizeToFit()
//
//            // atacco alla parte superiore della TableView la searchBar
//            self.tableView.tableHeaderView = controller.searchBar
//
//            // restituisco il controller creato
//            return controller
//        })()
//    }
    
//    func updateSearchResults(for searchController: UISearchController) {
////        print("Sto per iniziare una ricerca")
//        self.filtraContenuti(testoCercato: searchController.searchBar.text!, scope: "Tutti")
//    }
//
//    func filtraContenuti(testoCercato: String, scope: String = "Tutti") {
////        listaFiltrata.removeAll(keepingCapacity: true)
////        for x in listaParagrafi {
//////                if (scope == "Tutti") {
////            if((x.localizedLowercase.range(of: testoCercato.localizedLowercase) != nil)) {
////                        print("aggiungo \(x) alla listaFiltrata")
////                        listaFiltrata.append(x)
////                    }
//////                }
////            self.tableView.reloadData()
////        }
//    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
//        guard let controller = self.resultSearchController else {
//            return 0
//        }
//
//        if controller.isActive {
//            return 1
//        } else {
//            return self.listaSections.count
//        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.paragrafi.count
        // se la search bar viene visualizzata allora ritorno il numero di elementi della lista filtrata se no quelli della lista paragrafi
//        guard let controller = self.resultSearchController else {
//            return 0
//        }
//
//        if controller.isActive {
//            return self.listaFiltrata.count
//        } else {
//            return 1
//        }
    }

//    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 100
//    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = ParagrafoTableViewCell()
        if type(of: paragrafi[indexPath.row].paragrafo[0]) == Firma.self {
            cell = tableView.dequeueReusableCell(withIdentifier: "CellaParagrafiFirma", for: indexPath) as! ParagrafoTableViewCell
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "CellaParagrafi", for: indexPath) as! ParagrafoTableViewCell
        }
        
        cell.lblTitolo.text = paragrafi[indexPath.row].titolo
        // se viene la Search Bar è attiva allora utilizza l'elemento con indice visualizzato a partire dalla listra Filtrata
//        if self.resultSearchController!.isActive {
//            testoCella = listaFiltrata[indexPath.row]
//        } else {
//            //ricavo un elemento della lista in posizione row (il num di riga) e lo conservo
//            testoCella = self.listaParagrafi[indexPath.row]
//        }
        cell.backView.layer.cornerRadius = 20
        cell.isEmptyView.layer.cornerRadius = 7
        if paragrafi[indexPath.row].isEmpty{
            cell.isEmptyView.backgroundColor = UIColor.systemOrange
            cell.disclosureView.tintColor = UIColor.systemOrange
        } else {
            cell.isEmptyView.backgroundColor = UIColor.systemGreen
            cell.disclosureView.tintColor = UIColor.systemGreen
        }
        
        return cell
    }
    
//    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?{
//        return documento[section].titolo
//        guard let controller = self.resultSearchController else {
//            return nil
//        }
//        if controller.isActive {
//            return nil
//        } else {
//            return self.listaSections[section]
//        }
//    } // fixed font style. use custom view (UILabel) if you want something different

    
    //quando viene attivato il segue setta il titolo della View di Dettaglio in base al nome
    //dell'elemento o cercato o selezionato
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "dettagliSegue" {
            let dettagliViewController = segue.destination as! DettagliTableViewController
            let indexPath = self.tableView.indexPathForSelectedRow
            if self.resultSearchController?.isActive ?? false {
                dettagliViewController.paragrafo = self.listaFiltrata[indexPath?.row ?? 0]
            } else {
                dettagliViewController.paragrafo = self.paragrafi[indexPath?.row ?? 0]
                dettagliViewController.completionHandler = {
//                    parCallback in self.paragrafi[parCallback.numero] = parCallback
//                    self.tableView.reloadRows(at: [IndexPath(row: parCallback.numero, section: 0)], with: UITableView.RowAnimation.fade)
                    parCallback in
                    self.tableView.reloadRows(at: [(indexPath ?? IndexPath(row: 0, section: 0))], with: UITableView.RowAnimation.fade)
                }
            }
        } else if segue.identifier == "pdfSegue" {
            let pdfViewController = segue.destination as! PDFViewController
            pdfViewController.documento = self.documento
        } else if segue.identifier == "firmaSegue" {
            let firmaViewController = segue.destination as! SignatureViewController
            let indexPath = self.tableView.indexPathForSelectedRow
                firmaViewController.paragrafo = self.paragrafi[indexPath?.row ?? 0]
                firmaViewController.completionHandler = {
//                    parCallback in self.paragrafi[parCallback.numero] = parCallback
//                    self.tableView.reloadRows(at: [IndexPath(row: parCallback.numero, section: 0)], with: UITableView.RowAnimation.fade)
                    parCallback in
                    self.tableView.reloadRows(at: [(indexPath ?? IndexPath(row: 0, section: 0))], with: UITableView.RowAnimation.fade)
                }
        }
    }
    
    
}
