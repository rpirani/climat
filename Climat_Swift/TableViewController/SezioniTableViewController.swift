//
//  SezioniTableViewController.swift
//  Climat_Swift
//
//  Created by Rudi on 10/04/2020.
//  Copyright © 2020 Rudi. All rights reserved.
//

import UIKit

class SezioniTableViewController: UITableViewController {
    
    var documento = Documento()
    var enmTipoDocumento = Documento.enumTipoDoc.nessuno
    var sezioni = [Sezione]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = documento.titolo
        sezioni = documento.sezioni
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    @IBAction func showActionSheet(_ sender: Any) {
        let optionMenu = UIAlertController(title: nil, message: "", preferredStyle: .actionSheet)
            
        let deleteAction = UIAlertAction(title: "Nuovo documento", style: .default, handler: chiediNuovoDocumento(alert:))
        let sendAction = UIAlertAction(title: "Invia documento", style: .default, handler: inviaDocumento(alert:))
        let saveAction = UIAlertAction(title: "Sposta in Storico", style: .default, handler: salvaDocumento(alert:))
        
        let cancelAction = UIAlertAction(title: "Annulla", style: .cancel)
            
        optionMenu.addAction(sendAction)
        if self.documento.nomeFile == "" {
            optionMenu.addAction(deleteAction)
            optionMenu.addAction(saveAction)
        }
        optionMenu.addAction(cancelAction)
            
        self.present(optionMenu, animated: true, completion: nil)
    }
    func creaNuovoDocumento() {
        self.documento = self.documento.creaNuovoDocumento(enmTipoDocumento: self.documento.enmTipoDoc)
        self.sezioni = self.documento.sezioni
        self.tableView.reloadData()
    }
    
    func chiediNuovoDocumento(alert: UIAlertAction!) {
        let alert = UIAlertController(title: "Vuoi creare un nuovo documento?", message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Si", style: .destructive, handler: {
            action in self.creaNuovoDocumento()
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func inviaDocumento(alert: UIAlertAction!) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "PDFViewController") as! PDFViewController
        vc.documento = self.documento
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func salvaDocumento(alert: UIAlertAction!) {
        // Se il documento ha gia un nome assegnato, lo sovrascrivo con quel nome
        if self.documento.nomeFile != "" {
            if !self.documento.salva(nomeFile: documento.nomeFile) {
                // Alert salvataggio non riuscito
                self.mostraAlert(titolo: "Attenzione", messaggio: "Salvataggio dati non riuscito")
            } else {
                // Se il salvataggio avviene correttamente, creo un nuovo documento, in modo da spostare il doc appena creato in Storico
                self.creaNuovoDocumento()
            }
            return
        }
        // Se il documento NON ha un nome assegnato, chiedo il nome e lo salvo con il nome immesso dall'utente
        let alert = UIAlertController(title: "Salvataggio documento", message: "Assegna un nome al documento", preferredStyle: .alert)

        alert.addTextField { (textField) in
            //textField.text = "Some default text"
        }

        alert.addAction(UIAlertAction(title: "Annulla", style: .cancel))
        alert.addAction(UIAlertAction(title: "Sposta", style: .default, handler: { [weak alert] (_) in
            let text = alert?.textFields![0].text
            // Se il testo è vuoto
            if text == "" {
                self.mostraAlert(titolo: "Attenzione", messaggio: "Assegnare un nome al documento")
            } else {
                // Altrimenti salvo
                if !self.documento.salva(nomeFile: text!) {
                    // Alert salvataggio non riuscito
                    self.mostraAlert(titolo: "Attenzione", messaggio: "Salvataggio dati non riuscito")
                } else {
                    // Se il salvataggio avviene correttamente, creo un nuovo documento, in modo da spostare il doc appena creato in Storico
                    self.creaNuovoDocumento()
                }
            }
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func mostraAlert(titolo: String, messaggio: String){
        let alert = UIAlertController(title: titolo, message: messaggio, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.sezioni.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellaSezioni", for: indexPath) as! SezioneTableViewCell

        cell.lblTitolo.text = sezioni[indexPath.row].titolo
        cell.backView.layer.cornerRadius = 20
        cell.isEmptyView.layer.cornerRadius = 7
        cell.isEmptyView.backgroundColor = UIColor.systemBlue
        cell.disclosureView.tintColor = UIColor.systemBlue
        
        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */


// MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "paragrafiSegue" {
            let paragrafiViewController = segue.destination as! ParagrafiTableViewController
            paragrafiViewController.documento = self.documento
            paragrafiViewController.indSezione = self.tableView.indexPathForSelectedRow?.row ?? 0
        }
    }

}
