//
//  StoricoTableViewController.swift
//  Climat_Swift
//
//  Created by Rudi on 29/04/2020.
//  Copyright © 2020 Rudi. All rights reserved.
//

import UIKit

class StoricoTableViewController: UITableViewController {

    var documenti = [Documento()]
    var directoryContents = [URL]()
//    var
    
    override func viewDidLoad() {
        super.viewDidLoad()
        documenti.removeAll()
        
//        let dirs = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.allDomainsMask, true) as? [String]
        // Get the document directory url
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

        do {
            // Get the directory contents urls (including subfolders urls)
            directoryContents = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil)
//            print(directoryContents)
            
//            for filePath in directoryContents {
//                let data = try Data(contentsOf: filePath)//6
//                documenti.append(try (NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? Documento ?? Documento()))
//            }
//
            for filePath in directoryContents {
                let doc = Documento()
                documenti.append(doc.caricaDocumentoDaPercorso(filePath: filePath)!)
            }

        } catch {
            mostraAlert(titolo: "Errore", messaggio: error.localizedDescription)
        }
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    @objc func pushToParagrafi(indDoc : NSInteger) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ParagrafiTableViewController") as! ParagrafiTableViewController
        vc.documento = self.documenti[indDoc]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func pushToSezione(indDoc : NSInteger) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "SezioniTableViewController") as! SezioniTableViewController
        vc.documento = self.documenti[indDoc]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - Mostra Alert
    
    func mostraAlert(titolo: String, messaggio: String){
        let alert = UIAlertController(title: titolo, message: messaggio, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return documenti.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellaStorico", for: indexPath) as! StoricoTableViewCell

        cell.backView.tag = indexPath.row
        cell.backView.layer.cornerRadius = 7
        cell.lblTitolo.text = documenti[indexPath.row].nomeFile
        if documenti[indexPath.row].enmTipoDoc == .librettoCompleto {
            cell.imgTipoDoc.image = UIImage.init(systemName: "doc.on.doc")
        }

        return cell
    }
    
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if documenti[indexPath.row].enmTipoDoc == .librettoCompleto {
            pushToSezione(indDoc: indexPath.row)
        } else if documenti[indexPath.row].enmTipoDoc == .rapportoControlloTipo1 {
            pushToParagrafi(indDoc: indexPath.row)
        }
    }
    

    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    

    func rimuoviDocDaStorico(indice: NSInteger) {
        if documenti[indice].rimuoviQuestoDocumento() {
            // Rimosso correttamente
            documenti.remove(at: indice)
            directoryContents.remove(at: indice)
            tableView.deleteRows(at: [IndexPath.init(row: indice, section: 0)], with: .fade)
        } else {
            mostraAlert(titolo: "Errore", messaggio: "Questo documento non è stato rimosso")
        }
    }
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            let alert = UIAlertController(title: "Vuoi rimuovere questo documento?", message: "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Si", style: .destructive, handler: {
                action in self.rimuoviDocDaStorico(indice: indexPath.row)
            }))
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
