//
//  DettagliTableViewController.swift
//  Climat_Swift
//
//  Created by Rudi on 03/02/2020.
//  Copyright © 2020 Rudi. All rights reserved.
//

import UIKit


class DettagliTableViewController: UITableViewController, TestoCellDelegate, RadioCellDelegate, NumeroCellDelegate, DataCellDelegate {
    
    var tfTag = 0;
    var paragrafo = Paragrafo()
    var completionHandler:((Paragrafo) -> ())?
    var listaMapTextFieldTag = [Int]()
    var datePicker: UIDatePicker?
    
    // MARK: - Delegate Controlli
    
    func valueChangedData(datePicker: UIDatePicker, cell: DataTableViewCell) {
        let indexPath = self.tableView.indexPath(for: cell)
        if indexPath != nil {
            if type(of: paragrafo.paragrafo[indexPath!.section]) == MyDate.self {
                (paragrafo.paragrafo[indexPath!.section] as! MyDate).valore = datePicker.date
            } else if type(of: paragrafo.paragrafo[indexPath!.section]) == MyTime.self {
                (paragrafo.paragrafo[indexPath!.section] as! MyTime).valore = datePicker.date
            }
            
            isEmptyFalse()
        }
    }
    
    func valueChangedTesto(textField: UITextField, cell: TestoTableViewCell) {
        let indexPath = self.tableView.indexPath(for: cell)
        (paragrafo.paragrafo[indexPath!.section] as! Testo).valore = textField.text!
        isEmptyFalse()
    }

//    func valueChanged(switchControl: UISwitch, cell: FlagTableViewCell) {
//        let indexPath = self.tableView.indexPath(for: cell)
//        (paragrafo.paragrafo[indexPath!.section] as! Flag).valore = switchControl.isOn
//        isEmptyFalse()
//    }

    //6. Implement Delegate Method
    func valueChanged(segmentedControl: UISegmentedControl, cell: RadioTableViewCell) {        //Get the indexpath of cell where button was tapped
        let indexPath = self.tableView.indexPath(for: cell)
//        print(indexPath!.section)
        (paragrafo.paragrafo[indexPath!.section] as! Radio).valore = segmentedControl.selectedSegmentIndex
        isEmptyFalse()
    }
    
    func valueChanged(textField: UITextField, cell: NumeroTableViewCell) {
        let indexPath = self.tableView.indexPath(for: cell)
        (paragrafo.paragrafo[indexPath!.section] as! Numero).valore = textField.text!
        isEmptyFalse()
    }
    
    // Registro la modifica in modo da segnalare che almeno un campo è stato completato
    func isEmptyFalse(){
        if paragrafo.isEmpty{
            paragrafo.isEmpty = false
        }
    }
    
//    @objc func dateChanged(datePicker: UIDatePicker){
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "dd/MM/yyyy"
//        datePicker.in
//        txtDatePicker.text = dateFormatter.string(from: datePicker.date)
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        datePicker = UIDatePicker()
        datePicker?.datePickerMode = .date
        
//        datePicker?.addTarget(self, action: #selector(self.dateChanged(datePicker:)), for: .valueChanged)
        // Se la TBKeyboard non è stata istanziata, la istanzio
        loadToolBarKeyboard()
        
        // Popolo la listaMapTextFieldTag che mi serve per sapere quale cella ha un UITextField a cui assegnare il TAG (per lo scorrimento dei vari textfield tramite tastiera)
        tfTag = 1;
        for i in 0 ..< paragrafo.paragrafo.count {
            if type(of: paragrafo.paragrafo[i]) == Testo.self || type(of: paragrafo.paragrafo[i]) == Numero.self || type(of: paragrafo.paragrafo[i]) == MyDate.self || type(of: paragrafo.paragrafo[i]) == MyTime.self {
                listaMapTextFieldTag.append(tfTag)
                tfTag += 1
            } else {
                listaMapTextFieldTag.append(0)
            }
        }
        
        // Ricarico le celle
//        tableView.reloadData()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        _ = completionHandler?(paragrafo)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return paragrafo.paragrafo.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if type(of: paragrafo.paragrafo[section]) == Info.self {
            return 0
        }
        return 1
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if type(of: paragrafo.paragrafo[indexPath.section]) == Testo.self {
            var cellTesto = TestoTableViewCell()
            cellTesto = tableView.dequeueReusableCell(withIdentifier: "CellaTesto", for: indexPath) as! TestoTableViewCell
            cellTesto.delegate = self
            cellTesto.txtField.inputAccessoryView = tbKeyboard
            cellTesto.txtField.delegate = self
            cellTesto.txtField.tag = listaMapTextFieldTag[indexPath.section]
            cellTesto.txtField.text = (paragrafo.paragrafo[indexPath.section] as! Testo).valore
            return cellTesto
            
//        } else if type(of: paragrafo.paragrafo[indexPath.section]) == Flag.self {
//            var cellFlag = FlagTableViewCell()
//            cellFlag = tableView.dequeueReusableCell(withIdentifier: "CellaFlag", for: indexPath) as! FlagTableViewCell
//            cellFlag.delegate = self
//            cellFlag.switch.isOn = (paragrafo.paragrafo[indexPath.section] as! Flag).valore
//            return cellFlag
//
        } else if type(of: paragrafo.paragrafo[indexPath.section]) == Numero.self {
            var cellNumero = NumeroTableViewCell()
            cellNumero = tableView.dequeueReusableCell(withIdentifier: "CellaNumero", for: indexPath) as! NumeroTableViewCell
            cellNumero.delegate = self
            cellNumero.txtField.inputAccessoryView = tbKeyboard
            cellNumero.txtField.delegate = self
            cellNumero.txtField.tag = listaMapTextFieldTag[indexPath.section]
            cellNumero.txtField.text = (paragrafo.paragrafo[indexPath.section] as! Numero).valore
            return cellNumero
            
        } else if type(of: paragrafo.paragrafo[indexPath.section]) == Radio.self {
            var cellRadio = RadioTableViewCell()
            cellRadio = tableView.dequeueReusableCell(withIdentifier: "CellaRadio", for: indexPath) as! RadioTableViewCell
            //7. delegate view controller instance to the cell
            cellRadio.delegate = self
            // Imposto il numero corretto di radio button, con relativo testo
            let radio = (paragrafo.paragrafo[indexPath.section] as! Radio)
            cellRadio.radioBtn.removeAllSegments()
            for i in 0 ..< radio.txtValore.count {
                cellRadio.radioBtn.insertSegment(withTitle: radio.txtValore[i], at: i, animated: false)
            }
            // Imposto la selezione del radio button
            if radio.valore > -1 && radio.valore < radio.txtValore.count {
                cellRadio.radioBtn.selectedSegmentIndex = radio.valore
            } else {
                cellRadio.radioBtn.selectedSegmentIndex = -1
            }
            return cellRadio
            
        } else if type(of: paragrafo.paragrafo[indexPath.section]) == MyDate.self {
            let cellData = tableView.dequeueReusableCell(withIdentifier: "CellaData", for: indexPath) as! DataTableViewCell
//            datePicker?.date = (paragrafo.paragrafo[indexPath.section] as! MyDate).valore
            cellData.delegate = self
            cellData.txtDatePicker.text = (paragrafo.paragrafo[indexPath.section] as! MyDate).getDataString()
            cellData.txtDatePicker.tag = listaMapTextFieldTag[indexPath.section]
            cellData.txtDatePicker.inputAccessoryView = tbKeyboard
            cellData.txtDatePicker.inputView = datePicker
            datePicker?.addTarget(cellData, action: #selector(cellData.dateChanged(datePicker:)), for: .valueChanged)

//            cellData.btnData.setTitle(MyData.getDataString(of: (paragrafo.paragrafo[indexPath.section] as! MyData).valore, with: "dd/MM/yyyy"), for: UIControl.State.normal)
            return cellData
            
        } else if type(of: paragrafo.paragrafo[indexPath.section]) == MyTime.self {
            let cellData = tableView.dequeueReusableCell(withIdentifier: "CellaData", for: indexPath) as! DataTableViewCell
//            datePicker?.date = (paragrafo.paragrafo[indexPath.section] as! MyTime).valore
            cellData.delegate = self
            cellData.txtDatePicker.text = (paragrafo.paragrafo[indexPath.section] as! MyTime).getTimeString()
            cellData.txtDatePicker.tag = listaMapTextFieldTag[indexPath.section]
            cellData.txtDatePicker.inputAccessoryView = tbKeyboard
            cellData.txtDatePicker.inputView = datePicker
            datePicker?.addTarget(cellData, action: #selector(cellData.dateChanged(datePicker:)), for: .valueChanged)

//            cellData.btnData.setTitle(MyData.getDataString(of: (paragrafo.paragrafo[indexPath.section] as! MyData).valore, with: "dd/MM/yyyy"), for: UIControl.State.normal)
            return cellData
        }

        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?{
        return paragrafo.paragrafo[section].descrizione
    } // fixed font style. use custom view (UILabel) if you want something different
    
    // setto la data nel picker in base al UITextField selezionato
    @IBAction func textFieldEditingBegin(_ sender: UITextField) {
        for i in 0 ..< listaMapTextFieldTag.count {
            if listaMapTextFieldTag[i] == sender.tag {
                if type(of: paragrafo.paragrafo[i]) == MyDate.self {
                    datePicker?.datePickerMode = .date
                    datePicker?.setDate((paragrafo.paragrafo[i] as! MyDate).valore, animated: true)
                } else if type(of: paragrafo.paragrafo[i]) == MyTime.self {
                    datePicker?.datePickerMode = .time
                    datePicker?.setDate((paragrafo.paragrafo[i] as! MyTime).valore, animated: true)
                }
                
                return
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
