//
//  DataTableViewCell.swift
//  Climat_Swift
//
//  Created by Rudi on 03/03/2020.
//  Copyright © 2020 Rudi. All rights reserved.
//

import UIKit

protocol DataCellDelegate: AnyObject {
    func valueChangedData(datePicker: UIDatePicker, cell: DataTableViewCell)
}

class DataTableViewCell: UITableViewCell {

    @IBOutlet weak var txtDatePicker: UITextField!
    
    weak var delegate: DataCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @objc func dateChanged(datePicker: UIDatePicker){
        if txtDatePicker.isFirstResponder {
            let dateFormatter = DateFormatter()
            if datePicker.datePickerMode == .date {
                dateFormatter.dateFormat = "dd/MM/yyyy"
            } else {
                dateFormatter.dateFormat = "HH:mm"
            }
            
            txtDatePicker.text = dateFormatter.string(from: datePicker.date)
            delegate?.valueChangedData(datePicker: datePicker, cell: self)
        }
    }

}
