//
//  NumeroTableViewCell.swift
//  Climat_Swift
//
//  Created by Rudi on 05/02/2020.
//  Copyright © 2020 Rudi. All rights reserved.
//

import UIKit

protocol NumeroCellDelegate: AnyObject {
    func valueChanged(textField: UITextField, cell: NumeroTableViewCell)
}

class NumeroTableViewCell: UITableViewCell {

    @IBOutlet weak var txtField: UITextField!
    
    weak var delegate: NumeroCellDelegate?
    @IBAction func valueChanged(sender: UITextField) {
        delegate?.valueChanged(textField: sender, cell: self)
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
