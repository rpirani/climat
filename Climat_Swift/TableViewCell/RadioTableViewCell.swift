//
//  RadioTableViewCell.swift
//  Climat_Swift
//
//  Created by Rudi on 05/02/2020.
//  Copyright © 2020 Rudi. All rights reserved.
//

import UIKit

//1. delegate method
protocol RadioCellDelegate: AnyObject {
    func valueChanged(segmentedControl: UISegmentedControl, cell: RadioTableViewCell)
}

class RadioTableViewCell: UITableViewCell {

    @IBOutlet weak var radioBtn: UISegmentedControl!
    
    //2. create delegate variable
    weak var delegate: RadioCellDelegate?
    
    //3. assign this action to close button
    @IBAction func valueChanged(sender: UISegmentedControl) {
        //4. call delegate method
        //check delegate is not nil with `?`
        delegate?.valueChanged(segmentedControl: sender, cell: self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
