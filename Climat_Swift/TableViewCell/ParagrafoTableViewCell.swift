//
//  ParagrafoTableViewCell.swift
//  Climat_Swift
//
//  Created by Rudi on 09/02/2020.
//  Copyright © 2020 Rudi. All rights reserved.
//

import UIKit

class ParagrafoTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitolo: UILabel!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var isEmptyView: UIView!
    @IBOutlet weak var disclosureView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
