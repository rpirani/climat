//
//  StoricoTableViewCell.swift
//  Climat_Swift
//
//  Created by Rudi on 29/04/2020.
//  Copyright © 2020 Rudi. All rights reserved.
//

import UIKit

class StoricoTableViewCell: UITableViewCell {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var lblTitolo: UILabel!
    @IBOutlet weak var imgTipoDoc: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
