//
//  SezioneTableViewCell.swift
//  Climat_Swift
//
//  Created by Rudi on 10/04/2020.
//  Copyright © 2020 Rudi. All rights reserved.
//

import UIKit

class SezioneTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitolo: UILabel!
    @IBOutlet weak var isEmptyView: UIView!
    @IBOutlet weak var disclosureView: UIImageView!
    @IBOutlet weak var backView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
