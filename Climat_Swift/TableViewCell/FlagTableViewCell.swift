//
//  FlagTableViewCell.swift
//  Climat_Swift
//
//  Created by Rudi on 05/02/2020.
//  Copyright © 2020 Rudi. All rights reserved.
//

import UIKit

protocol FlagCellDelegate: AnyObject {
    func valueChanged(switchControl: UISwitch, cell: FlagTableViewCell)
}

class FlagTableViewCell: UITableViewCell {

    @IBOutlet weak var `switch`: UISwitch!
    
    weak var delegate: FlagCellDelegate?
    @IBAction func valueChanged(sender: UISwitch) {
        delegate?.valueChanged(switchControl: sender, cell: self)
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
