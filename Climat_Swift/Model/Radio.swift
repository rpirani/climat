//
//  Radio.swift
//  Climat_Swift
//
//  Created by Rudi on 03/02/2020.
//  Copyright © 2020 Rudi. All rights reserved.
//
import UIKit

class Radio : InputObject {
//    var descrizione = String()
    var txtValore = [String]()
    var valore = Int()
    var location = [CGPoint()]
    
    init(descrizione: String, txtValore: [String], valore: Int, location: [CGPoint]) {
        super.init(descrizione: descrizione)
//        self.descrizione = descrizione
        self.txtValore = txtValore
        self.valore = valore
        self.location = location
        self.item_type = ItemType.radio
    }
    
    
    // MARK: - NSCoding Implementation
    
    enum CodingKeys: String, CodingKey {
      case descrizione
      case txtValore
      case valore
      case location
        case item_type
    }
    
    override func encode(to encoder: Encoder) throws {
      var container = encoder.container(keyedBy: CodingKeys.self)
      try container.encode(descrizione, forKey: .descrizione)
      try container.encode(txtValore, forKey: .txtValore)
      try container.encode(valore, forKey: .valore)
      try container.encode(location, forKey: .location)
        try container.encode(item_type.rawValue, forKey: .item_type)
        
      let superencoder = container.superEncoder()
      try super.encode(to: superencoder)
    }
    required init(from decoder: Decoder) throws {
      let container = try decoder.container(keyedBy: CodingKeys.self)
        let superdecoder = try container.superDecoder()
        try super.init(from: superdecoder)
        descrizione = try container.decode(String.self, forKey: .descrizione)
        txtValore = try container.decode([String].self, forKey: .txtValore)
        valore = try container.decode(Int.self, forKey: .valore)
        location = try container.decode([CGPoint].self, forKey: .location)
        self.item_type = ItemType.radio
    }
    
    
//    func encode(with coder: NSCoder) {
//        coder.encode(descrizione, forKey: Keys.descrizione.rawValue)
//        coder.encode(txtValore, forKey: Keys.txtValore.rawValue)
//        coder.encode(valore, forKey: Keys.valore.rawValue)
//        coder.encode(location, forKey: Keys.location.rawValue)
//    }
//
//    required convenience init?(coder: NSCoder) {
//        let _descrizione = coder.decodeObject(forKey: Keys.descrizione.rawValue) as! String
//        let _txtValore = coder.decodeObject(forKey: Keys.txtValore.rawValue) as! [String]
//        let _valore = coder.decodeCInt(forKey: Keys.valore.rawValue)
//        let _location = coder.decodeObject(forKey: Keys.location.rawValue) as! [CGPoint]
//
//        self.init(descrizione: _descrizione, txtValore: _txtValore, valore: Int(_valore), location: _location)
//    }
    
    override func draw() {
        if valore > -1 && valore < location.count {
            let rect = CGRect(x: location[valore].x, y:  location[valore].y - 9, width: 10, height: 14)
        
            let font = UIFont.init(name: "Helvetica", size: 9)// .systemFont(ofSize: 10, weight: .bold)
    //        let font = UIFont.systemFont(ofSize: 10, weight: .light)

            let paragraphStyle:NSMutableParagraphStyle = NSMutableParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
            paragraphStyle.alignment = .left
            paragraphStyle.lineBreakMode = NSLineBreakMode.byWordWrapping

            let textColor = UIColor.darkGray

            let textFontAttributes = [
                NSAttributedString.Key.font: font,
                NSAttributedString.Key.foregroundColor: textColor,
                NSAttributedString.Key.paragraphStyle: paragraphStyle
            ]
            let flagX = "x"
            let text:NSString = (NSString)(string: flagX)

            text.draw(in: rect, withAttributes: textFontAttributes as [NSAttributedString.Key : Any])
        }
    }
    
}
