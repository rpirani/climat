//
//  InputObject.swift
//  Climat_Swift
//
//  Created by Rudi on 06/07/2020.
//  Copyright © 2020 Rudi. All rights reserved.
//

import Foundation

import UIKit

class InputObject : NSObject, Codable {

    public enum ItemType: String, Decodable {
        case testo
        case radio
        case numero
        case firma
        case data
        case ora
        case info
        case nessuno
    }

    
    var descrizione = String()
    var item_type: ItemType
//    var flagX = "x"
    
    enum CodingKeys: String, CodingKey {
        case descrizione
        case item_type
    }
    
    init(descrizione: String) {
        self.descrizione = descrizione
        self.item_type = .nessuno
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(descrizione, forKey: .descrizione)
        try container.encode(item_type.rawValue, forKey: .item_type)
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
//      self.init(descrizione: try container.decode(String.self, forKey: .descrizione))
        self.descrizione = try container.decode(String.self, forKey: .descrizione)
        self.item_type = try container.decode(ItemType.self, forKey: .item_type)
        
        let tag = try container.decode(ItemType.self, forKey: .item_type)

        let singleValueContainer = try decoder.singleValueContainer()
        switch tag {
        case .testo:
            // if it's not a Foo, throw and blame the server guy
            let prova = try singleValueContainer.decode(Testo.self)
            try Testo.init(from: decoder)

        case .radio:
            // if it's not a Foo, throw and blame the server guy
//            self = try singleValueContainer.decode(Testo.self)
            
//            let prova = try container.decode
            let prova2 = try Radio.init(from: decoder)
//            prova.descrizione = "aa"
            prova2.descrizione = "aaa"
            
        default:
            try Testo.init(from: decoder)
            // this tag is unknown, or known but we don't care
        }
    }
    
    func draw() {}
    
}
