//
//  Sezione.swift
//  Climat_Swift
//
//  Created by Rudi on 03/02/2020.
//  Copyright © 2020 Rudi. All rights reserved.
//

import Foundation

class Sezione : NSObject, Codable {
    var numero = Int()
    var titolo = String()
    var paragrafi = [Paragrafo]()
    
    init(numero: Int, titolo: String, paragrafi: [Paragrafo]) {
        self.numero = numero
        self.titolo = titolo
        self.paragrafi = paragrafi
        
        // Encode
//        let dog = Dog(name: "Rex", owner: "Etgar")
//
//        let jsonEncoder = JSONEncoder()
//        let jsonData = try jsonEncoder.encode(dog)
//        let json = String(data: jsonData, encoding: String.Encoding.utf16)
    }
    
    override init() {
    }
    
    // MARK: - NSCoding Implementation
    
    enum CodingKeys: String, CodingKey {
      case numero
      case titolo
      case paragrafi
    }
    
    func encode(to encoder: Encoder) throws {
      var container = encoder.container(keyedBy: CodingKeys.self)
      try container.encode(numero, forKey: .numero)
      try container.encode(titolo, forKey: .titolo)
      try container.encode(paragrafi, forKey: .paragrafi)
    }
    required convenience init(from decoder: Decoder) throws {
      let container = try decoder.container(keyedBy: CodingKeys.self)
      self.init(numero: try container.decode(Int.self, forKey: .numero),
                titolo: try container.decode(String.self, forKey: .titolo),
                paragrafi: try container.decode([Paragrafo].self, forKey: .paragrafi))
    }
    
    
    
//    func encode(with coder: NSCoder) {
//        coder.encode(numero, forKey: Keys.numero.rawValue)
//        coder.encode(titolo, forKey: Keys.titolo.rawValue)
//        coder.encode(paragrafi, forKey: Keys.paragrafi.rawValue)
//    }
//
//    required convenience init?(coder: NSCoder) {
//        let _numero = coder.decodeCInt(forKey: Keys.numero.rawValue)
//        let _titolo = coder.decodeObject(forKey: Keys.titolo.rawValue) as! String
//        let _paragrafi = coder.decodeObject(forKey: Keys.paragrafi.rawValue) as! [Paragrafo]
//
//        self.init(numero: Int(_numero), titolo: _titolo, paragrafi: _paragrafi)
//    }
    
}

