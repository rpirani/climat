//
//  Info.swift
//  Climat_Swift
//
//  Created by Rudi on 09/03/2020.
//  Copyright © 2020 Rudi. All rights reserved.
//

import UIKit

class Info : InputObject  {
//var descrizione = String()
    
    override init(descrizione: String) {
        super.init(descrizione: descrizione)
//        self.descrizione = descrizione
        self.item_type = ItemType.info
    }
    
    // MARK: - NSCoding Implementation
    
    enum CodingKeys: String, CodingKey {
      case descrizione
      case item_type
    }
    
//    func encode(with coder: NSCoder) {
//        coder.encode(descrizione, forKey: Keys.descrizione.rawValue)
//    }
    
    // MARK: - JSON
//    init(title:String,price:Double, quantity:Int) {
//      self.title = title
//      self.price = price
//      self.quantity = quantity
//    }
    override func encode(to encoder: Encoder) throws {
      var container = encoder.container(keyedBy: CodingKeys.self)
      try container.encode(descrizione, forKey: .descrizione)
      try container.encode(item_type.rawValue, forKey: .item_type)
      
      let superencoder = container.superEncoder()
      try super.encode(to: superencoder)
    }
    required init(from decoder: Decoder) throws {
      let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let superdecoder = try container.superDecoder()
        try super.init(from: superdecoder)
        
      descrizione = try container.decode(String.self, forKey: .descrizione)
    }
    
    override func draw() {}
    
}
/*
    Testo(descrizione: "Altro", valore: "", location: CGPoint(x: -123, y: -123)),
 
    Testo(descrizione: "", valore: "", location: CGPoint(x: -123, y: -123)),

    Radio(descrizione: "", txtValore: ["SI", "NO"], valore: -1, location: [CGPoint(x: -123, y: -123), CGPoint(x: OUT_X, y: 0)]),

    MyDate(descrizione: "Data", valore: NSDate.now, location: CGPoint(x: -123, y: -123)),

    Numero(descrizione: "", valore: "", location: CGPoint(x: -123, y: -123)),

Paragrafo(numero: 0, titolo: "", pag: , paragrafo: [
    
]),

Info(descrizione: ""),


Sezione(numero: 0, titolo: "", paragrafi: [
])
 
- Radio 4
 Radio(descrizione: "", txtValore: ["", "", "", ""], valore: -1, location: [CGPoint(x: -123, y: -123), CGPoint(x: -123, y: -123), CGPoint(x: -123, y: -123), CGPoint(x: -123, y: -123)]),
 
- Radio 3
 Radio(descrizione: "", txtValore: ["", "", ""], valore: -1, location: [CGPoint(x: -123, y: -123), CGPoint(x: -123, y: -123), CGPoint(x: -123, y: -123)]),
 
 Paragrafo(numero: 0, titolo: "Firma del responsabile impianto", pag: , paragrafo: [
     Firma(descrizione: "Firma Responsabile", location: CGPoint(x: -123, y: -123))
 ])
*/
