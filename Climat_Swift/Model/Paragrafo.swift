//
//  Paragrafo.swift
//  Climat_Swift
//
//  Created by Rudi on 03/02/2020.
//  Copyright © 2020 Rudi. All rights reserved.
//

import Foundation

class Paragrafo : NSObject, Codable {
    var numero = Int()
    var isEmpty = Bool()
    var titolo = String()
    var paragrafo = [InputObject]()
    var pagina = Int()
    
    init(numero: Int, isEmpty: Bool = true, titolo: String, pag: Int, paragrafo: [InputObject]) {
        self.numero = numero
        self.isEmpty = isEmpty
        self.titolo = titolo
        self.paragrafo = paragrafo
        self.pagina = pag
    }
        
    override init() {
    }
    
    // MARK: - NSCoding Implementation
    
    enum CodingKeys: String, CodingKey {
      case numero
      case isEmpty
      case titolo
      case paragrafo
      case pagina
    }
    
    func encode(to encoder: Encoder) throws {
      var container = encoder.container(keyedBy: CodingKeys.self)
      try container.encode(numero, forKey: .numero)
      try container.encode(isEmpty, forKey: .isEmpty)
      try container.encode(titolo, forKey: .titolo)
      try container.encode(paragrafo, forKey: .paragrafo)
      try container.encode(pagina, forKey: .pagina)
    }
    required init(from decoder: Decoder) throws {
      let container = try decoder.container(keyedBy: CodingKeys.self)
      numero = try container.decode(Int.self, forKey: .numero)
        isEmpty = try container.decode(Bool.self, forKey: .isEmpty)
        titolo = try container.decode(String.self, forKey: .titolo)
        pagina = try container.decode(Int.self, forKey: .pagina)
        
        let singleValueContainer = try decoder.singleValueContainer()
        paragrafo = try container.decode([InputObject].self, forKey: .paragrafo).map({ item in
            switch item.item_type {
            case .testo:
                return item as! Testo
                
            case .radio:
                return try singleValueContainer.decode(Radio.self)
                
            case .numero:
                return item as! Numero
                
            case .firma:
                return item as! Firma
                
            case .data:
                return item as! MyDate
                
            case .ora:
                return item as! MyTime
                
            case .info:
                return item as! Info
                
            case .nessuno:
                return InputObject(descrizione: "nessuno")
            }
            
        })
    }
    
    
    
//    func encode(with coder: NSCoder) {
//        coder.encode(numero, forKey: Keys.numero.rawValue)
//        coder.encode(isEmpty, forKey: Keys.isEmpty.rawValue)
//        coder.encode(titolo, forKey: Keys.titolo.rawValue)
//        coder.encode(paragrafo, forKey: Keys.paragrafo.rawValue)
//        coder.encode(pagina, forKey: Keys.pagina.rawValue)
//    }
//
//    required convenience init?(coder: NSCoder) {
//        let _numero = coder.decodeCInt(forKey: Keys.numero.rawValue)
//        let _isEmpty = coder.decodeBool(forKey: Keys.isEmpty.rawValue)
//        let _titolo = coder.decodeObject(forKey: Keys.titolo.rawValue) as! String
//        let _paragrafo = coder.decodeObject(forKey: Keys.paragrafo.rawValue) as! [InputObject]
//        let _pagina = coder.decodeCInt(forKey: Keys.pagina.rawValue)
//
//        self.init(numero: Int(_numero), isEmpty: _isEmpty, titolo: _titolo, pag: Int(_pagina), paragrafo: _paragrafo)
//    }
    
}

