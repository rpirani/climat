//
//  Firma.swift
//  Climat_Swift
//
//  Created by Rudi on 27/02/2020.
//  Copyright © 2020 Rudi. All rights reserved.
//

import Foundation
import UIKit

class Firma : InputObject {
//var descrizione = String()
var firmaImage = UIImage()
var location = CGPoint()

    init(descrizione: String, location: CGPoint) {
        super.init(descrizione: descrizione)
//        self.descrizione = descrizione
        self.location = location
        self.firmaImage = UIImage.init()
        self.item_type = ItemType.firma
    }
    
    init(descrizione: String, location: CGPoint, firmaImage: UIImage) {
        super.init(descrizione: descrizione)
//        self.descrizione = descrizione
        self.location = location
        self.firmaImage = firmaImage
    }
    
    public func setFirma(immagine: UIImage){
        self.firmaImage = immagine
    }
    
    // MARK: - NSCoding Implementation
    
    enum CodingKeys: String, CodingKey {
      case descrizione
      case firmaImage
      case location
      case item_type
    }
    
    override func encode(to encoder: Encoder) throws {
      var container = encoder.container(keyedBy: CodingKeys.self)
      try container.encode(descrizione, forKey: .descrizione)
      try container.encode(firmaImage.pngData(), forKey: .firmaImage)
      try container.encode(location, forKey: .location)
      try container.encode(item_type.rawValue, forKey: .item_type)
        
      let superencoder = container.superEncoder()
      try super.encode(to: superencoder)
    }
    required init(from decoder: Decoder) throws {
      let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let superdecoder = try container.superDecoder()
        try super.init(from: superdecoder)
        
        descrizione = try container.decode(String.self, forKey: .descrizione)
        location = try container.decode(CGPoint.self, forKey: .location)
        firmaImage = UIImage(data: try container.decode(Data.self, forKey: .firmaImage)) ?? UIImage()
        self.item_type = ItemType.firma
    }
    
    
    
    
//    func encode(with coder: NSCoder) {
//        coder.encode(descrizione, forKey: Keys.descrizione.rawValue)
//        coder.encode(firmaImage, forKey: Keys.firmaImage.rawValue)
//        coder.encode(location, forKey: Keys.location.rawValue)
//    }
//
//    required convenience init?(coder: NSCoder) {
//        let _descrizione = coder.decodeObject(forKey: Keys.descrizione.rawValue) as! String
//        let _firmaImage = coder.decodeObject(forKey: Keys.firmaImage.rawValue) as! UIImage
//        let _location = coder.decodeCGPoint(forKey: Keys.location.rawValue)
//
//        self.init(descrizione: _descrizione, location: _location, firmaImage: _firmaImage)
//    }
    
    override func draw() {
        if firmaImage.size != CGSize.zero{
            let rect = CGRect(x: location.x+1, y:  location.y - 35.5, width: firmaImage.size.height/10, height: firmaImage.size.width/10)
           // Draw image on top of page
           (firmaImage.rotate(radians: .pi/2) ?? UIImage()).draw(in: rect)
        }
    }
    
}
