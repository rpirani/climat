//
//  Numero.swift
//  Climat_Swift
//
//  Created by Rudi on 05/02/2020.
//  Copyright © 2020 Rudi. All rights reserved.
//
import UIKit

class Numero : InputObject {
//var descrizione = String()
var valore = String()
var location = CGPoint()

    init(descrizione: String, valore: String, location: CGPoint) {
        super.init(descrizione: descrizione)
//        self.descrizione = descrizione
        self.valore = valore
        self.location = location
        self.item_type = ItemType.numero
    }
    
    // MARK: - NSCoding Implementation
    
    enum CodingKeys: String, CodingKey {
      case descrizione
      case valore
      case location
      case item_type
    }
    
    
    override func encode(to encoder: Encoder) throws {
      var container = encoder.container(keyedBy: CodingKeys.self)
      try container.encode(descrizione, forKey: .descrizione)
      try container.encode(valore, forKey: .valore)
      try container.encode(location, forKey: .location)
      try container.encode(item_type.rawValue, forKey: .item_type)
        
        let superencoder = container.superEncoder()
        try super.encode(to: superencoder)
    }
    required init(from decoder: Decoder) throws {
      let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let superdecoder = try container.superDecoder()
        try super.init(from: superdecoder)
        descrizione = try container.decode(String.self, forKey: .descrizione)
                valore = try container.decode(String.self, forKey: .valore)
                location = try container.decode(CGPoint.self, forKey: .location)
                self.item_type = ItemType.numero
    }
    
    
//    func encode(with coder: NSCoder) {
//        coder.encode(descrizione, forKey: Keys.descrizione.rawValue)
//        coder.encode(valore, forKey: Keys.valore.rawValue)
//        coder.encode(location, forKey: Keys.location.rawValue)
//    }
//
//    required convenience init?(coder: NSCoder) {
//        let _descrizione = coder.decodeObject(forKey: Keys.descrizione.rawValue) as! String
//        let _valore = coder.decodeObject(forKey: Keys.valore.rawValue) as! String
//        let _location = coder.decodeCGPoint(forKey: Keys.location.rawValue)
//
//        self.init(descrizione: _descrizione, valore: _valore, location: _location)
//    }
    
    override func draw() {
        if valore != "" {
            let rect = CGRect(x: location.x+1, y: location.y - 9.5, width: 100, height: 15)
            let font = UIFont.init(name: "Helvetica", size: 9)// .systemFont(ofSize: 10, weight: .bold)
    //        let font = UIFont.systemFont(ofSize: 10, weight: .light)

            let paragraphStyle:NSMutableParagraphStyle = NSMutableParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
            paragraphStyle.alignment = .center
            paragraphStyle.lineBreakMode = NSLineBreakMode.byWordWrapping

            let textColor = UIColor.darkGray

            let textFontAttributes = [
                NSAttributedString.Key.font: font,
                NSAttributedString.Key.foregroundColor: textColor,
                NSAttributedString.Key.paragraphStyle: paragraphStyle
            ]

            let text:NSString = (NSString)(string: valore)

            text.draw(in: rect, withAttributes: textFontAttributes as [NSAttributedString.Key : Any])
        }
    }
    
}
