//
//  Data.swift
//  Climat_Swift
//
//  Created by Rudi on 03/03/2020.
//  Copyright © 2020 Rudi. All rights reserved.
//

import UIKit

class MyDate : InputObject {
//var descrizione = String()
var valore = Date.init(timeIntervalSince1970: TimeInterval.init())
var location = CGPoint()

    init(descrizione: String, valore: Date, location: CGPoint) {
        super.init(descrizione: descrizione)
//        self.descrizione = descrizione
        self.valore = valore
        self.location = location
        self.item_type = ItemType.data
    }
    
    func getDataString() -> String {
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "dd/MM/yyyy"//"yyyy-MM-dd HH:mm:ss"

        return formatter.string(from: self.valore) // string purpose I add here
    }
    
    static func getDataString(of myDate: Date, with format: String) -> String {
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = format//"yyyy-MM-dd HH:mm:ss"

        return formatter.string(from: myDate) // string purpose I add here
        // convert your string to date
    }
    
    // MARK: - NSCoding Implementation
    
    enum CodingKeys: String, CodingKey {
      case descrizione
      case valore
      case location
      case item_type
    }
    
    override func encode(to encoder: Encoder) throws {
      var container = encoder.container(keyedBy: CodingKeys.self)
      try container.encode(descrizione, forKey: .descrizione)
      try container.encode(valore, forKey: .valore)
      try container.encode(location, forKey: .location)
      try container.encode(item_type.rawValue, forKey: .item_type)
        
        let superencoder = container.superEncoder()
        try super.encode(to: superencoder)
    }
    required init(from decoder: Decoder) throws {
      let container = try decoder.container(keyedBy: CodingKeys.self)
        
      let superdecoder = try container.superDecoder()
      try super.init(from: superdecoder)
      
      descrizione = try container.decode(String.self, forKey: .descrizione)
        valore = try container.decode(Date.self, forKey: .valore)
        location = try container.decode(CGPoint.self, forKey: .location)
        self.item_type = ItemType.data
    }
    
    
    
    
//    func encode(with coder: NSCoder) {
//        coder.encode(descrizione, forKey: Keys.descrizione.rawValue)
//        coder.encode(valore, forKey: Keys.valore.rawValue)
//        coder.encode(location, forKey: Keys.location.rawValue)
//    }
//
//    required convenience init?(coder: NSCoder) {
//        let _descrizione = coder.decodeObject(forKey: Keys.descrizione.rawValue) as! String
//        let _valore = coder.decodeObject(forKey: Keys.valore.rawValue) as! Date
//        let _location = coder.decodeCGPoint(forKey: Keys.location.rawValue)
//
//        self.init(descrizione: _descrizione, valore: _valore, location: _location)
//    }
    
    override func draw() {
        if valore != (Date.init(timeIntervalSince1970: TimeInterval.init(exactly: 0)!)) {
            let rect = CGRect(x: location.x - 2, y: location.y - 9.5, width: 100, height: 15)
            let font = UIFont.init(name: "Helvetica", size: 9)// .systemFont(ofSize: 10, weight: .bold)
    //        let font = UIFont.systemFont(ofSize: 10, weight: .light)

            let paragraphStyle:NSMutableParagraphStyle = NSMutableParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
            paragraphStyle.alignment = .left
            paragraphStyle.lineBreakMode = NSLineBreakMode.byWordWrapping

            let textColor = UIColor.darkGray

            let textFontAttributes = [
                NSAttributedString.Key.font: font,
                NSAttributedString.Key.foregroundColor: textColor,
                NSAttributedString.Key.paragraphStyle: paragraphStyle
            ]

            let text:NSString = (NSString)(string: getDataString())

            text.draw(in: rect, withAttributes: textFontAttributes as [NSAttributedString.Key : Any])
        }
    }
    
}

class MyTime : InputObject {
//var descrizione = String()
var valore = Date.init(timeIntervalSince1970: TimeInterval.init())
var location = CGPoint()

    init(descrizione: String, valore: Date, location: CGPoint) {
        super.init(descrizione: descrizione)
//        self.descrizione = descrizione
        self.valore = valore
        self.location = location
        self.item_type = ItemType.ora
    }
    
    func getTimeString() -> String {
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "HH:mm"//"yyyy-MM-dd HH:mm:ss"

        return formatter.string(from: self.valore) // string purpose I add here
    }
    
    // MARK: - NSCoding Implementation
    
    enum CodingKeys: String, CodingKey {
      case descrizione
      case valore
      case location
      case item_type
    }
    
    override func encode(to encoder: Encoder) throws {
      var container = encoder.container(keyedBy: CodingKeys.self)
      try container.encode(descrizione, forKey: .descrizione)
      try container.encode(valore, forKey: .valore)
      try container.encode(location, forKey: .location)
      try container.encode(item_type.rawValue, forKey: .item_type)
      
      let superencoder = container.superEncoder()
      try super.encode(to: superencoder)
    }
    required init(from decoder: Decoder) throws {
      let container = try decoder.container(keyedBy: CodingKeys.self)
        
      let superdecoder = try container.superDecoder()
      try super.init(from: superdecoder)
      
      descrizione = try container.decode(String.self, forKey: .descrizione)
        valore = try container.decode(Date.self, forKey: .valore)
        location = try container.decode(CGPoint.self, forKey: .location)
        self.item_type = ItemType.ora
    }
    
//    func encode(with coder: NSCoder) {
//        coder.encode(super.descrizione, forKey: Keys.descrizione.rawValue)
//        coder.encode(valore, forKey: Keys.valore.rawValue)
//        coder.encode(location, forKey: Keys.location.rawValue)
//    }
//
//    required convenience init?(coder: NSCoder) {
//        let _descrizione = coder.decodeObject(forKey: Keys.descrizione.rawValue) as! String
//        let _valore = coder.decodeObject(forKey: Keys.valore.rawValue) as! Date
//        let _location = coder.decodeCGPoint(forKey: Keys.location.rawValue)
//
//        self.init(descrizione: _descrizione, valore: _valore, location: _location)
//    }
    
    override func draw() {
        if valore != (Date.init(timeIntervalSince1970: TimeInterval.init(exactly: 0)!)) {
            let rect = CGRect(x: location.x+1, y: location.y - 10.5, width: 100, height: 15)
            let font = UIFont.init(name: "Helvetica", size: 9)// .systemFont(ofSize: 10, weight: .bold)
    //        let font = UIFont.systemFont(ofSize: 10, weight: .light)

            let paragraphStyle:NSMutableParagraphStyle = NSMutableParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
            paragraphStyle.alignment = .left
            paragraphStyle.lineBreakMode = NSLineBreakMode.byWordWrapping

            let textColor = UIColor.darkGray

            let textFontAttributes = [
                NSAttributedString.Key.font: font,
                NSAttributedString.Key.foregroundColor: textColor,
                NSAttributedString.Key.paragraphStyle: paragraphStyle
            ]

            let text:NSString = (NSString)(string: getTimeString())

            text.draw(in: rect, withAttributes: textFontAttributes as [NSAttributedString.Key : Any])
        }
    }
    
}
