//
//  ReMoFilesViewController.swift
//  Climat_Swift
//
//  Created by Rudi on 19/08/2020.
//  Copyright © 2020 Rudi. All rights reserved.
//

import UIKit
import FilesProvider

class ReMoFilesViewController: UIViewController, FileProviderDelegate {
    
    let server: URL = URL(string: "ftp://pi:raspberry@192.168.0.10/")!
    let username = "pi"
    let password = "raspberry"
    var ftp: FTPFileProvider?
    
    var fileNames = NSMutableArray()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let credential = URLCredential(user: username, password: password, persistence: .permanent)
        
        ftp = FTPFileProvider(baseURL: server, mode: .passive, credential: credential, cache: nil)
        
        ftp?.delegate = self as FileProviderDelegate
        
        updateFileNames()
    }
    
    func updateFileNames() {
        fileNames.removeAllObjects()
        ftp?.contentsOfDirectory(path: "files", completionHandler: {FileObject,Error in
            if Error != nil {
                print(Error?.localizedDescription ?? "Errore nell'ottenere la lista di file in ReMo")
            }
            for file in FileObject {
                self.fileNames.add(file.name)
                print(file.name)
            }
        })
    }
    
    @IBAction func getData(_ sender: Any) {
//        var object = [FileObject]
        
//        ftp?.contents(path: "prova2.pdf", completionHandler: {
//            contents, error in
//            if error != nil {
//                print(error?.localizedDescription)
//            }
//            if let contents = contents {
//                print(String(data: contents, encoding: .utf8))
//            }
//        })
    }
    
    func fileproviderSucceed(_ fileProvider: FileProviderOperations, operation: FileOperationType) {
        switch operation {
        case .copy(source: let source, destination: let dest):
            print("\(source) copied to \(dest).")
        case .remove(path: let path):
            print("\(path) has been deleted.")
        default:
            if let destination = operation.destination {
                print("\(operation.actionDescription) from \(operation.source) to \(destination) succeed.")
            } else {
                print("\(operation.actionDescription) on \(operation.source) succeed.")
            }
        }
    }
    
    func fileproviderFailed(_ fileProvider: FileProviderOperations, operation: FileOperationType, error: Error) {
        switch operation {
        case .copy(source: let source, destination: let dest):
            print("copying \(source) to \(dest) has been failed.")
        case .remove:
            print("file can't be deleted.")
        default:
            if let destination = operation.destination {
                print("\(operation.actionDescription) from \(operation.source) to \(destination) failed.")
            } else {
                print("\(operation.actionDescription) on \(operation.source) failed.")
            }
        }
    }
    
    func fileproviderProgress(_ fileProvider: FileProviderOperations, operation: FileOperationType, progress: Float) {
        switch operation {
        case .copy(source: let source, destination: let dest) where dest.hasPrefix("file://"):
            print("Downloading \(source) to \((dest as NSString).lastPathComponent): \(progress * 100) completed.")
        case .copy(source: let source, destination: let dest) where source.hasPrefix("file://"):
            print("Uploading \((source as NSString).lastPathComponent) to \(dest): \(progress * 100) completed.")
        case .copy(source: let source, destination: let dest):
            print("Copy \(source) to \(dest): \(progress * 100) completed.")
        default:
            break
        }
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
