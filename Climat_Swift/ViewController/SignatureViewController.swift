//
//  SignatureViewController.swift
//  Climat_Swift
//
//  Created by Rudi on 27/02/2020.
//  Copyright © 2020 Rudi. All rights reserved.
//

import UIKit

class SignatureViewController: UIViewController, SwiftSignatureViewDelegate {
    
    var paragrafo = Paragrafo()
    var completionHandler:((Paragrafo) -> ())?
    
    @IBAction func clearSignature(_ sender: UIButton) {
        signatureView.clear()
        paragrafo.isEmpty = true
        (self.paragrafo.paragrafo[0] as! Firma).setFirma(immagine: UIImage())
    }
    
    @IBOutlet weak var signatureView: SwiftSignatureView!
    
    func swiftSignatureViewDidTapInside(_ view: SwiftSignatureView) {
        
    }
    
    func swiftSignatureViewDidPanInside(_ view: SwiftSignatureView, _ pan: UIPanGestureRecognizer) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.signatureView.delegate = self
        self.signatureView.signature = (paragrafo.paragrafo[0] as! Firma).firmaImage
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if signatureView.signature != nil && signatureView.signature?.size != CGSize.zero {
            paragrafo.isEmpty = false
            (self.paragrafo.paragrafo[0] as! Firma).setFirma(immagine: signatureView.signature!)
        }
        _ = completionHandler?(paragrafo)        
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.allButUpsideDown, andRotateTo: UIInterfaceOrientation.portrait)

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
