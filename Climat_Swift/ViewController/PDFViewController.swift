//
//  PDFViewController.swift
//  Climat_Swift
//
//  Created by Rudi on 31/01/2020.
//  Copyright © 2020 Rudi. All rights reserved.
//

import UIKit
import WebKit
import MessageUI
import PDFKit
// Per ottenere SSID
import SystemConfiguration.CaptiveNetwork
import CoreLocation

class PDFViewController: UIViewController, MFMailComposeViewControllerDelegate, CLLocationManagerDelegate {

    var documento = Documento()
    var titoloPDF = String()
    let dataPdf = NSMutableData.init()
    // Inizializzo PDF View
    let pdfView = PDFView()
    let mailSubject = "climat.iot@gmail.com"
    var flagX = "x"
    // Per ottenere SSID
    var locationManager = CLLocationManager()
    let SSID_ReMo = "ReMo" // Nome della rete wifi con cui ReMo crea l'hotspot
    var SSID = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.title = MyDate.getDataString(of: documento.dataRevisione, with: "dd/MM/yyyy")
        titoloPDF = documento.titolo
        
        if documento.enmTipoDoc == .librettoCompleto {
            flagX = "X"
        }
        
        pdfView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(pdfView)
        pdfView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        pdfView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        pdfView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        pdfView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        
        drawDati(titoloPDF: titoloPDF)
        
        if let document = PDFDocument(data: dataPdf as Data) {
            pdfView.document = document
        }
    }

    
    func drawDati(titoloPDF: String)
    {
        let stringPath = Bundle.main.path(forResource: titoloPDF, ofType: "pdf")
        let pdf = CGPDFDocument(NSURL(fileURLWithPath: stringPath!))
        UIGraphicsBeginPDFContextToData(dataPdf, CGRect.zero, nil)

        var page_old =  0
        for sez in documento.sezioni {
        for par in sez.paragrafi {
            // Disegno la pagina del pdf solo la prima volta per ogni pagina
            if par.pagina != page_old {
                let page =  pdf?.page(at: par.pagina)

                let pageFrame = page?.getBoxRect(.mediaBox)

                UIGraphicsBeginPDFPageWithInfo(pageFrame!, nil)

                let ctx = UIGraphicsGetCurrentContext()

                // Draw existing page
                ctx!.saveGState()

                ctx!.scaleBy(x: 1, y: -1)

                ctx!.translateBy(x: 0, y: -pageFrame!.size.height)
                //CGContextTranslateCTM(ctx, 0, -pageFrame.size.height);
                ctx!.drawPDFPage(page!)
                ctx!.restoreGState()
                
                page_old = par.pagina
            }
            // Disegno i num pagina del Rapporto Controllo Tipo 1
            if page_old == 1 && documento.enmTipoDoc == Documento.enumTipoDoc.rapportoControlloTipo1 {
                drawText(testo: "1", rect: CGRect(x: 522, y: 14, width: 10, height: 15))
                drawText(testo: "1", rect: CGRect(x: 551, y: 14, width: 10, height: 15))
            }
//            let page = pdfView.document?.page(at: par.pagina-1)
            
            for tipo in par.paragrafo {
                tipo.draw()
//                // TESTO
//                if type(of: tipo) == Testo.self {
//                    let testo = (tipo as! Testo)
//                    if testo.valore != "" {
//                        testo.drawText(testo: testo.valore, rect: CGRect(x: testo.location.x+1, y: testo.location.y - 9.5, width: 300, height: 15))
//                    }
//                // RADIO
//                } else if type(of: tipo) == Radio.self {
//                    let radio = (tipo as! Radio)
//                    if radio.valore > -1 && radio.valore < radio.location.count {
//                        drawText(testo: flagX, rect: CGRect(x: radio.location[radio.valore].x, y:  radio.location[radio.valore].y - 9, width: 10, height: 14))
//                    }
//                // NUMERO
//                } else if type(of: tipo) == Numero.self {
//                    let numero = (tipo as! Numero)
//                    if numero.valore != "" {
//                        drawText(testo: numero.valore, rect: CGRect(x: numero.location.x+1, y: numero.location.y - 9.5, width: 100, height: 15), align: .center)
//                    }
//                // FIRMA
//                } else if type(of: tipo) == Firma.self {
//                    let firma = (tipo as! Firma)
//                    if firma.firmaImage.size != CGSize.zero{
//                        drawImage(image: firma.firmaImage.rotate(radians: .pi/2) ?? UIImage(), rect: CGRect(x: firma.location.x+1, y:  firma.location.y - 35.5, width: firma.firmaImage.size.height/10, height: firma.firmaImage.size.width/10))
////                        let imageAnnotation = ImageStampAnnotation(with: firma.firmaImage.rotate(radians: .pi/2), forBounds: CGRect(x: firma.location.x, y: 840.5 - firma.location.y, width: firma.firmaImage.size.height/10, height: firma.firmaImage.size.width/10), withProperties: nil)
////                        imageAnnotation.interiorColor = .white
////                        imageAnnotation.backgroundColor = .white
////                        imageAnnotation.color = .white
////                        page?.addAnnotation(imageAnnotation)
//                    }
//                // DATA
//                } else if type(of: tipo) == MyDate.self {
//                    let myData = (tipo as! MyDate)
//                    if myData.valore != (Date.init(timeIntervalSince1970: TimeInterval.init(exactly: 0)!)) {
//                        drawText(testo: myData.getDataString(), rect: CGRect(x: myData.location.x - 2, y: myData.location.y - 9.5, width: 100, height: 15))
//                    }
//                // ORA
//                } else if type(of: tipo) == MyTime.self {
//                let myTime = (tipo as! MyTime)
//                if myTime.valore != (Date.init(timeIntervalSince1970: TimeInterval.init(exactly: 0)!)) {
//                    drawText(testo: myTime.getTimeString(), rect: CGRect(x: myTime.location.x+1, y: myTime.location.y - 10.5, width: 100, height: 15))
//                }
//               }
            }
        }
        }
        
        UIGraphicsEndPDFContext()
    }
//
    func drawText(testo: String, rect: CGRect, align: NSTextAlignment = .left) {
        let font = UIFont.init(name: "Helvetica", size: 9)// .systemFont(ofSize: 10, weight: .bold)
//        let font = UIFont.systemFont(ofSize: 10, weight: .light)

        let paragraphStyle:NSMutableParagraphStyle = NSMutableParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        paragraphStyle.alignment = align
        paragraphStyle.lineBreakMode = NSLineBreakMode.byWordWrapping

        let textColor = UIColor.darkGray

        let textFontAttributes = [
            NSAttributedString.Key.font: font,
            NSAttributedString.Key.foregroundColor: textColor,
            NSAttributedString.Key.paragraphStyle: paragraphStyle
        ]

        let text:NSString = (NSString)(string: testo)

        text.draw(in: rect, withAttributes: textFontAttributes as [NSAttributedString.Key : Any])
    }
//
//    func drawImage(image: UIImage, rect: CGRect) {
//        // Draw image on top of page
//        let image = image
//        image.draw(in: rect)
//    }
//
    
    // MARK: - Invio Mail
    
    @IBAction func sendMail(_ sender: Any) {
        if MFMailComposeViewController.canSendMail() {
            var messaggio = "Documento: " + titoloPDF + "<br>"
            messaggio += "Data: " + MyDate.getDataString(of: documento.dataRevisione, with: "dd/MM/yyyy") + "<br>"
            messaggio += "Operatore: " + "Mario Rossi" + "<br>"
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([mailSubject])
            mail.setSubject(titoloPDF)
            mail.setMessageBody(messaggio, isHTML: true)
            //add attachment
            mail.addAttachmentData((pdfView.document?.dataRepresentation()!)!, mimeType: "application/pdf" , fileName: titoloPDF + ".pdf")
            present(mail, animated: true)
        } else {
            // show failure alert
            creaMostraAlert(controller: self, titolo: "Attenzione", messaggio: "Non hai configurato nessuna mail nelle impostazioni del tuo dispositivo", handlerOK: nil)
        }
    }

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        if let _ = error {
           self.dismiss(animated: true, completion: nil)
        }
        var messaggio = ""
        switch result {
           case .cancelled:
            messaggio = "Invio annullato"
           break
           case .sent:
            messaggio = "Mail inviata correttamente"
           break
           case .failed:
            messaggio = "Invio della mail non riuscito"
           break
           default:
           break
        }
        creaMostraAlert(controller: controller, titolo: messaggio, messaggio: "", handlerOK: {
                       action in controller.dismiss(animated: true)
                   })
    }
    
    func creaMostraAlert(controller: UIViewController, titolo: String, messaggio: String, handlerOK: ((UIAlertAction) -> Void)?) {
        let alert = UIAlertController(title: titolo, message: messaggio, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: handlerOK))
        controller.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Invio a dispositivo ReMo
    
    @IBAction func sendDevice(_ sender: Any) {
        test()
//        print("Nome File: " + documento.titolo + "_" + MyDate.getDataString(of: documento.dataRevisione, with: "yyyyMMdd"))
        // Verifico di essere collegato alla rete wifi ReMo
        if !checkSSID(){
            creaMostraAlert(controller: self, titolo: "Ops...", messaggio: "Sembra che tu non sia collegato alla rete WiFi Remo\r\nConnettiti tramite le Impostazioni e riprova", handlerOK: nil)
            return
        }
        // Mi connetto al server FTP e invio il documento
        let ftpup = FTPUpload(baseUrl: "192.168.0.10", userName: "pi", password: "raspberry", directoryPath: "files")

//        // prova
//        var path = getDocumentsDirectory().appendPathComponent("prova5.pdf")
//        pdfView.document?.write(to: path)
//        if let document = PDFDocument(url: path) {
//            pdfView.document = document
//        }
        
//        ftpup.send(data: (pdfView.document?.dataRepresentation()!)!, with: documento.titolo + "_" + MyDate.getDataString(of: documento.dataRevisione, with: "yyyyMMdd"), success: {(success) -> Void in
//            if !success {
//                print("Failed upload!")
//                self.creaMostraAlert(controller: self, titolo: "Ops...", messaggio: "Qualcosa è andato storto.\r\nDocumento non trasferito a ReMo", handlerOK: nil)
//            }
//            else {
//                print("file uploaded!")
//                self.creaMostraAlert(controller: self, titolo: "", messaggio: "Documento trasferito a ReMo", handlerOK: nil)
//            }
//        })
        ftpup.send(data: (pdfView.document?.dataRepresentation()!)!, with: "prova4.pdf", success: {(success) -> Void in
            if !success {
                print("Failed upload!")
                self.creaMostraAlert(controller: self, titolo: "Ops...", messaggio: "Qualcosa è andato storto.\r\nDocumento non trasferito a ReMo", handlerOK: nil)
            }
            else {
                print("file uploaded!")
                self.creaMostraAlert(controller: self, titolo: "", messaggio: "Documento trasferito a ReMo", handlerOK: nil)
            }
        })
    }
    
    func test(){
        var myUrl:URL = URL(string:"ftp://pi:raspberry@192.168.0.10/files")!
            var myRequest:URLRequest  = URLRequest(url: myUrl as URL)

            let session = URLSession.shared
            let dataTask = session.dataTask(with: myRequest,
            completionHandler: {(data, response, error) -> Void in
                if error != nil{
                    //print(error.debugDescription)
                }else{
                    let str = String(data: data!, encoding: String.Encoding.utf8)
                    print(str)
                }
        }) as URLSessionTask

        dataTask.resume()
    }
    
// MARK: - GET SSID
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            updateWiFi()
        } else if status == .denied {
        creaMostraAlert(controller: self, titolo: "Attenzione", messaggio: "Per connettersi a ReMo è necessario attivare la posizione quando l'app è in uso.\r\nAttiva la posizione tramite Impostazioni -> Privacy -> Servizi di localizzazione", handlerOK: nil)
        }
    }
    
    func updateWiFi() {
        SSID = getSSID() ?? ""
    }
    
    // Verifica che il dispositivo sia connesso alla rete wifi ReMo
    func checkSSID() -> Bool{
        if #available(iOS 13.0, *) {
            let status = CLLocationManager.authorizationStatus()
            if status == .authorizedWhenInUse {
                updateWiFi()
            } else if status == .denied {
                creaMostraAlert(controller: self, titolo: "Attenzione", messaggio: "Per connettersi a ReMo è necessario attivare la posizione quando l'app è in uso.\r\nAttiva la posizione tramite Impostazioni -> Privacy -> Servizi di localizzazione", handlerOK: nil)
            } else {
                creaMostraAlert(controller: self, titolo: "Attenzione", messaggio: "Per connettersi a ReMo è necessario attivare la posizione quando l'app è in uso", handlerOK: { action in self.richiediAutorizzazionePosizione() })
            }
        } else {
            updateWiFi()
        }
        return SSID == SSID_ReMo
    }
    
    func getSSID() -> String? {
        var ssid: String?
        if let interfaces = CNCopySupportedInterfaces() as NSArray? {
            for interface in interfaces {
                if let interfaceInfo = CNCopyCurrentNetworkInfo(interface as! CFString) as NSDictionary? {
                    ssid = interfaceInfo[kCNNetworkInfoKeySSID as String] as? String
                    break
                }
            }
        }
        return ssid
    }
    
    func richiediAutorizzazionePosizione(){
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
    }
    

}
