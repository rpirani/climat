// TODO
// - nome app: RE.MO.
// - import export dati tramite json e invio con mail
// - gestire dati personali dell'utente in modo da precompilare i moduli con essi (quali dati?)
// - gestire multipagina nel pdf
// - togliere la visualizzazione dell'orario, e quindi se il campo è vuoto non lo metto nel pdf
// - libretto completo inviare modo per mettere le coordinate
// - nel libretto completo creare un'ulteriore pagina in cui si raccolgono le schede
// - queste schede posso selezionarle per mandarle in pdf, le schede non selezionate rimuovo le pagine del pdf. Salvo cmq sempre tutte le schede, anche quelle non selezionate
// - possibilità di stampare con una stampante bluetooth e/o stampante di rete

// - creare uno storico in modo che salva il documento con il tasto salva. utente decide il nome, poi se lo ritrova nello storico che può ricaricare



//
//  ViewController.swift
//  Climat_Swift
//
//  Created by Rudi on 31/01/2020.
//  Copyright © 2020 Rudi. All rights reserved.
//

import UIKit


class HomeViewController: UIViewController {
    
    @IBOutlet weak var viewModuloTipo1: UIView!
    @IBOutlet weak var viewLibrettoCompleto: UIView!
    @IBOutlet weak var viewStorico: UIView!
    @IBOutlet weak var viewAnagrafica: UIView!
    
    var documento = Documento()
    var enmTipoDocumento = Documento.enumTipoDoc.nessuno
    
    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // GRAFICA
        overrideUserInterfaceStyle = .light
        self.navigationController?.overrideUserInterfaceStyle = .light
        viewModuloTipo1.layer.cornerRadius = 10
        viewLibrettoCompleto.layer.cornerRadius = 10
        viewStorico.layer.cornerRadius = 10
        viewAnagrafica.layer.cornerRadius = 10
        
        let touchParagrafi = UITapGestureRecognizer(target:self, action:#selector(self.pushToParagrafi))
        self.viewModuloTipo1.addGestureRecognizer(touchParagrafi)
        let touchLibrettoCompleto = UITapGestureRecognizer(target:self, action:#selector(self.pushToSezione))
        self.viewLibrettoCompleto.addGestureRecognizer(touchLibrettoCompleto)
        let touchStorico = UITapGestureRecognizer(target:self, action:#selector(self.pushToStorico))
        self.viewStorico.addGestureRecognizer(touchStorico)
        
        let touchAnagrafica = UITapGestureRecognizer(target:self, action:#selector(self.pushToAnagrafica))
        self.viewAnagrafica.addGestureRecognizer(touchAnagrafica)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //self.navigationController?.setNavigationBarHidden(true, animated: animated)
        super.viewWillAppear(animated)
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        super.viewWillDisappear(animated)
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.allButUpsideDown, andRotateTo: UIInterfaceOrientation.portrait)

    }
    
    @objc func pushToParagrafi(sender : UIView) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ParagrafiTableViewController") as! ParagrafiTableViewController
        vc.documento = self.documento.creaCaricaDocumento(enmTipoDocumento: .rapportoControlloTipo1)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func pushToSezione(sender : UIView) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "SezioniTableViewController") as! SezioniTableViewController
        vc.documento = self.documento.creaCaricaDocumento(enmTipoDocumento: .librettoCompleto)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func pushToStorico(sender : UIView) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "StoricoTableViewController") as! StoricoTableViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func pushToAnagrafica(sender : UIView) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ReMoFilesViewController") as! ReMoFilesViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //quando viene attivato il segue setta il titolo della View di Dettaglio in base al nome
    //dell'elemento o cercato o selezionato
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "rapportoTipo1Segue" {
//            let paragrafiViewController = segue.destination as! ParagrafiTableViewController
//            paragrafiViewController.documento = self.documento.creaCaricaDocumento(enmTipoDocumento: .rapportoControlloTipo1)
//
//        } else if segue.identifier == "librettoCompletoSegue" {
//            let sezioniViewController = segue.destination as! SezioniTableViewController
//            sezioniViewController.documento = self.documento.creaCaricaDocumento(enmTipoDocumento: .librettoCompleto)
//        }
//    }
    

}

//extension UINavigationController {
//
//    override open var shouldAutorotate: Bool {
//        get {
//            if let visibleVC = visibleViewController {
//                return visibleVC.shouldAutorotate
//            }
//            return super.shouldAutorotate
//        }
//    }
//
//    override open var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation{
//        get {
//            if let visibleVC = visibleViewController {
//                return visibleVC.preferredInterfaceOrientationForPresentation
//            }
//            return super.preferredInterfaceOrientationForPresentation
//        }
//    }
//
//    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask{
//        get {
//            if let visibleVC = visibleViewController {
//                return visibleVC.supportedInterfaceOrientations
//            }
//            return super.supportedInterfaceOrientations
//        }
//    }
//
//}
