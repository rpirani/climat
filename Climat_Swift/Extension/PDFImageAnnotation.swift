//
//  PDFImageAnnotation.swift
//  Climat_Swift
//
//  Created by Rudi on 29/02/2020.
//  Copyright © 2020 Rudi. All rights reserved.
//
import UIKit
import PDFKit

class ImageStampAnnotation: PDFAnnotation {
    var image: UIImage!
    // A custom init that sets the type to Stamp on default and assigns our Image variable
    init(with image: UIImage!, forBounds bounds: CGRect, withProperties properties: [AnyHashable : Any]?) {
      super.init(bounds: bounds, forType: PDFAnnotationSubtype.stamp,  withProperties: properties)
      self.image = image
    }
    
    required init?(coder aDecoder: NSCoder) {
     fatalError("init(coder:) has not been implemented")
    }

    override func draw(with box: PDFDisplayBox, in context: CGContext)   {
      // Get the CGImage of our image
      guard let cgImage = self.image.cgImage else { return }
      // Draw our CGImage in the context of our PDFAnnotation bounds
      context.draw(cgImage, in: self.bounds)
    }
}

// Rotazione Immagine
extension UIImage {
    func rotate(radians: Float) -> UIImage? {
        var newSize = CGRect(origin: CGPoint.zero, size: self.size).applying(CGAffineTransform(rotationAngle: CGFloat(radians))).size
        // Trim off the extremely small float value to prevent core graphics from rounding it up
        newSize.width = floor(newSize.width)
        newSize.height = floor(newSize.height)

        UIGraphicsBeginImageContextWithOptions(newSize, false, self.scale)
        let context = UIGraphicsGetCurrentContext()!

        // Move origin to middle
        context.translateBy(x: newSize.width/2, y: newSize.height/2)
        // Rotate around middle
        context.rotate(by: CGFloat(radians))
        // Draw the image at its center
        self.draw(in: CGRect(x: -self.size.width/2, y: -self.size.height/2, width: self.size.width, height: self.size.height))

        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage
    }
}
