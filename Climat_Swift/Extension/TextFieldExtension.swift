//
//  TextFieldExtension.swift
//  Climat_Swift
//
//  Created by Rudi on 03/03/2020.
//  Copyright © 2020 Rudi. All rights reserved.
//
import UIKit

extension UIView {
    
    // go thru this view's subviews and look for the current first responder
    func findFirstResponder() -> UIResponder? {
    
        // if self is the first responder, return it
        // (this is from the recursion below)
        if isFirstResponder {
            return self
        }
        
        for v in subviews {
            if v.isFirstResponder == true {
                return v
            }
            if let fr = v.findFirstResponder() { // recursive
                return fr
            }
        }
        
        // no first responder
        return nil
    }
}
var tbKeyboard : UIToolbar?
extension UIViewController : UITextFieldDelegate {
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        loadToolBarKeyboard()
        // set the tool bar as this text field's input accessory view
        textField.inputAccessoryView = tbKeyboard
        return true
    }
    
    public func loadToolBarKeyboard() {
        // if there's no tool bar, create it
        if tbKeyboard == nil {
            tbKeyboard = UIToolbar.init(frame: CGRect.init(x: 0, y: 0,
                            width: self.view.frame.size.width, height: 44))
            let bbiPrev = UIBarButtonItem.init(title: "",
                            style: .plain, target: self, action: #selector(doBtnPrev))
            bbiPrev.tintColor = .black
            bbiPrev.image = UIImage.init(systemName: "arrowtriangle.up")
            let bbiSpacerFix = UIBarButtonItem(barButtonSystemItem: .fixedSpace,
                            target: nil, action: nil)
            bbiSpacerFix.width = 20;
            let bbiNext = UIBarButtonItem.init(title: "", style: .plain,
                            target: self, action: #selector(doBtnNext))
            bbiNext.tintColor = .black
            bbiNext.image = UIImage.init(systemName: "arrowtriangle.down")
            let bbiSpacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                            target: nil, action: nil)
            let bbiSubmit = UIBarButtonItem.init(title: "Fine", style: .plain,
                            target: self, action: #selector(doBtnSubmit))
            bbiSubmit.tintColor = .black
            tbKeyboard?.items = [bbiPrev, bbiSpacerFix, bbiNext, bbiSpacer, bbiSubmit]
        }
    }
    
    
    // search view's subviews
       // if no view is passed in, start w/ the self.view
       func findTextField(withTag tag : Int,
                          inViewsSubviewsOf view : UIView? = nil) -> UITextField? {
           for v in view?.subviews ?? self.view.subviews {
               
               // found a match? return it
               if v is UITextField, v.tag == tag {
                   return (v as! UITextField)
               }
               else if v.subviews.count > 0 { // recursive
                   if let tf = findTextField(withTag: tag, inViewsSubviewsOf: v) {
                       return tf
                   }
               }
           }
           return nil // not found
       }
       
       // make the next (or previous if next=false) text field the first responder
       func makeTFFirstResponder(next : Bool) -> Bool {
           
           // find the current first responder (text field)
           if let fr = self.view.findFirstResponder() as? UITextField {
           
               // find the next (or previous) text field based on the tag
               if let tf = findTextField(withTag: fr.tag + (next ? 1 : -1)) {
                   tf.becomeFirstResponder()
                   return true
               }
           }
           return false
       }
    
    @objc func doBtnPrev(_ sender: Any) {
        let _ = makeTFFirstResponder(next: false)
    }
    
    @objc func doBtnNext(_ sender: Any) {
        let _ = makeTFFirstResponder(next: true)
    }

//    // delegate method
//    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
////        tableView.becomeFirstResponder()
//        // when user taps Return, make the next text field first responder
//        if makeTFFirstResponder(next: true) == false {
//            // if it fails (last text field), submit the form
//            submitForm()
//        }
//
//        return false
//    }
    
    @objc func doBtnSubmit(_ sender: Any) {
        submitForm()
    }

    func submitForm() {
        self.view.endEditing(true)
        // override me
    }
}
